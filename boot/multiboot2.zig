//! Multiboot2 compliant boot sequence
//! Defines '_start' symbol as start of execution
//! Section '.boot' should be in the beginning of the ELF executable

const std = @import("std");
const cpu = @import("arch").x86.cpu;
const layout = @import("arch").x86.layout;

const bootloader_magic = 0x36d76289;

const MultiBoot = packed struct {
    magic: u32 = header_magic,
    arch: u32,
    length: u32,
    checksum: u32,
    type: u16,
    flags: u16 = header_flags,
    size: u32,

    const header_magic = 0xe85250d6;
    const header_flags = 0;
};

export const multiboot align(8) linksection(".boot") = MultiBoot{
    .arch = 0, // i386
    .length = @sizeOf(@TypeOf(MultiBoot)),
    .checksum = std.math.maxInt(u32) - (MultiBoot.header_magic + MultiBoot.header_flags + @sizeOf(@TypeOf(MultiBoot))) + 1,
    .type = 0,
    .size = 8, // TODO: Shouldn't be hard coded
};

// TODO: Shouldn't be defined here
var stack_bytes: [16 * 1024]u8 align(16) linksection(".bss") = undefined;

export fn _start() callconv(.Naked) noreturn {
    cpu.disableInterrupts();
    cpu.symbolAddrInRegister(.ecx, "__kernel_stack_head");
    cpu.moveRegister(.ecx, .ebp);
    cpu.moveRegister(.ecx, .esp);
    const magic = cpu.getRegister(.eax);
    const boot = cpu.getRegister(.ebx);
    cpu.clearFlags();
    @call(.{ .stack = layout.kernel_stack[0..] }, multibootEntry, .{ magic, boot });
}

fn multibootEntry(magic: u32, addr: u32) noreturn {
    _ = addr;
    if (magic != bootloader_magic)
        @panic("wrong multiboot magic number");
    kernelMain();
}

extern fn kernelMain() noreturn;
