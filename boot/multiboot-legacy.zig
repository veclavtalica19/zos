//! Multiboot compliant boot sequence
//! Defines '_start' symbol as start of execution
//! Section '.boot' should be in the beginning of the ELF executable

const std = @import("std");
const cpu = @import("arch").x86.cpu;
const layout = @import("arch").x86.layout;

// TODO: Define way to intersect boot structure provided by multiboot

const bootloader_magic = 0x2badb002;

const MultiBoot = packed struct {
    magic: u32 = header_magic,
    flags: u32 = header_flags,
    checksum: u32,

    const header_magic = 0x1badb002;
    const header_flags = 0;
};

export const multiboot align(4) linksection(".boot") = MultiBoot{
    .checksum = std.math.maxInt(u32) - (MultiBoot.header_magic + MultiBoot.header_flags) + 1,
};

export fn _start() callconv(.Naked) noreturn {
    cpu.disableInterrupts();
    cpu.symbolAddrInRegister(.ecx, "__kernel_stack_head");
    cpu.moveRegister(.ecx, .ebp);
    cpu.moveRegister(.ecx, .esp);
    const magic = cpu.getRegister(.eax);
    const boot = cpu.getRegister(.ebx);
    cpu.clearFlags();
    @call(.{ .stack = layout.kernel_stack[0..] }, multibootEntry, .{ magic, boot });
}

fn multibootEntry(magic: u32, addr: u32) noreturn {
    _ = addr;
    if (magic != bootloader_magic)
        @panic("wrong multiboot magic number");
    kernelMain();
}

extern fn kernelMain() noreturn;
