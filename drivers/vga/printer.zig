//! VGA Text mode printer
//! Its a "singleton" by design, meaning that nobody should be able to instance it

// https://en.wikipedia.org/wiki/VGA_text_mode

const std = @import("std");
const control_code = std.ascii.control_code;
const interface = @import("../printer.zig");

/// Must be initialized
pub var printer: VgaPrinter = undefined;

// TODO: Watch that one initialization matches deinitialization, as we're operating on global state here
pub fn init() void {
    printer = std.mem.zeroInit(VgaPrinter, .{});
    printer.buffer = VgaPrinter.buffer_base[0..(printer.width * printer.height)];
}

// TODO: Init method, as its not guaranteed that VGA text mode is active at all times, or available even
const VgaPrinter = struct {
    // TODO: Ability to specify desired text mode
    const buffer_base = @intToPtr([*]volatile u16, 0xb8000);
    // TODO: Detect exact mode and size
    // Dimensions in characters
    buffer: []volatile u16,
    width: u16 = 80,
    height: u16 = 25,
    row: u16 = 0,
    column: u16 = 0,
    color: u8 = initColor(.white, .black),

    pub fn getDims(self: *@This()) interface.Dims {
        return .{ .width = self.width, .height = self.height };
    }

    pub fn clear(self: *@This()) void {
        var y: usize = 0;
        while (y < self.height) : (y += 1) {
            var x: usize = 0;
            while (x < self.width) : (x += 1) {
                putCharAt(self, ' ', self.color, x, y);
            }
        }
    }

    pub fn putChar(self: *@This(), char: u8) void {
        if (char < 0x20) {
            // Control Characters
            switch (char) {
                control_code.LF => {
                    self.column = 0;
                    self.row = if (self.row == self.height) 0 else self.row + 1;
                },
                control_code.CR => {
                    self.column = 0;
                },
                else => {},
            }
        } else {
            // Printable Characters
            putCharAt(self, char, self.color, self.column, self.row);
            self.column += 1;
            if (self.column == self.width) {
                self.column = 0;
                self.row += 1;
                if (self.row == self.height)
                    self.row = 0;
            }
        }
    }

    inline fn putCharAt(self: *@This(), char: u8, color: u8, x: usize, y: usize) void {
        const index = x + (y * self.width);
        self.buffer[index] = initVgaChar(char, color);
    }

    pub fn setColor(self: *@This(), fg: u8, bg: u8) void {
        self.color = initColor(@intToEnum(VgaColor, fg), @intToEnum(VgaColor, bg));
    }

    pub fn setPos(self: *@This(), row: u16, column: u16) void {
        self.row = row - (self.height * (row / self.height));
        self.column = column - (self.width * (column / self.width));
    }

    const VgaColor = enum(u4) {
        black,
        blue,
        green,
        cyan,
        red,
        magenta,
        brown,
        light_grey,
        dark_grey,
        light_blue,
        light_green,
        light_cyan,
        light_red,
        light_magenta,
        light_brown,
        white,
    };

    inline fn initColor(fg: VgaColor, bg: VgaColor) u8 {
        return @as(u8, @enumToInt(fg)) | (@as(u8, @enumToInt(bg)) << 4);
    }

    inline fn initVgaChar(char: u8, color: u8) u16 {
        return char | (@as(u16, color) << 8);
    }
};
