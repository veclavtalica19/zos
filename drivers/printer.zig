//! Generic printer interface, based on character output with color support
const std = @import("std");

// TODO: @This() based methods to automatically push state into the virtual table calls
// TODO: Method for printing slices instead of individual chars. We could potentially only implement slicing even, as its uncommon to need single char output
// TODO: It probably have to state which ABI is used for calls in virtual table
pub const Dims = struct { width: u16, height: u16 };

pub const Printer = struct {
    getDims: fn (state: *anyopaque) Dims,
    clear: fn (state: *anyopaque) void,
    putChar: fn (state: *anyopaque, char: u8) void,
    setColor: fn (state: *anyopaque, bg: Color, fg: Color) void,
    setPos: fn (state: *anyopaque, row: u16, column: u16) void,

    /// Printer should not outlive state on which it is dependent
    state: *anyopaque,

    /// Writer should not outlive printer's state on which it is dependent
    pub fn writer(self: @This()) Writer {
        return Writer{ .context = self };
    }

    /// Wraps printer implementation to generic printer interface
    pub fn wrap(state_ptr: anytype) Printer {
        const state_type = @typeInfo(@TypeOf(state_ptr)).Pointer.child;
        const fieldInfo = std.meta.fieldInfo;
        return .{
            .state = state_ptr,
            .getDims = @ptrCast(fieldInfo(@This(), .getDims).field_type, state_type.getDims),
            .clear = @ptrCast(fieldInfo(@This(), .clear).field_type, state_type.clear),
            .putChar = @ptrCast(fieldInfo(@This(), .putChar).field_type, state_type.putChar),
            .setColor = @ptrCast(fieldInfo(@This(), .setColor).field_type, state_type.setColor),
            .setPos = @ptrCast(fieldInfo(@This(), .setPos).field_type, state_type.setPos),
        };
    }

    pub inline fn stateGetDims(self: @This()) Dims {
        return self.getDims(self.state);
    }

    pub inline fn stateClear(self: @This()) void {
        self.clear(self.state);
    }

    pub inline fn statePutChar(self: @This(), char: u8) void {
        self.putChar(self.state, char);
    }

    pub inline fn stateSetColor(self: @This(), bg: Color, fg: Color) void {
        self.setColor(self.state, bg, fg);
    }

    pub inline fn stateSetPos(self: @This(), row: u16, column: u16) void {
        self.setPos(self.state, row, column);
    }
};

// TODO: We might initialize writer from pointer to printer, which will reduce amount of copying when passing over function stack frames
// On other hand, this will make lifetime control on said stack harder, as you will need to control one more thing
pub const Writer = std.io.Writer(Printer, WriterErrors, write);
const WriterErrors = error{};

fn write(self: Printer, string: []const u8) WriterErrors!usize {
    for (string) |char| {
        self.statePutChar(char);
    }
    return string.len;
}

pub const Color = enum(u8) {
    // VGA compatibles
    black,
    blue,
    green,
    cyan,
    red,
    magenta,
    brown,
    light_grey,
    dark_grey,
    light_blue,
    light_green,
    light_cyan,
    light_red,
    light_magenta,
    light_brown,
    white,
};
