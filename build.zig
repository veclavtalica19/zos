const std = @import("std");

// TODO: There's probably more problematic features we need to turn off
const disabled_features = features: {
    const Feature = std.Target.x86.Feature;
    var features = std.Target.Cpu.Feature.Set.empty;
    features.addFeature(@enumToInt(Feature.sse));
    features.addFeature(@enumToInt(Feature.sse2));
    features.addFeature(@enumToInt(Feature.avx));
    features.addFeature(@enumToInt(Feature.avx2));
    break :features features;
};

const enabled_features = features: {
    const Feature = std.Target.x86.Feature;
    var features = std.Target.Cpu.Feature.Set.empty;
    features.addFeature(@enumToInt(Feature.x87));
    break :features features;
};

const default_target = std.zig.CrossTarget{
    .cpu_arch = .i386,
    .os_tag = .freestanding,
    .abi = .none,
    .cpu_features_sub = disabled_features,
    .cpu_features_add = enabled_features,
};

pub fn build(b: *std.build.Builder) !void {
    const target = b.standardTargetOptions(.{.default_target = default_target});
    const mode = b.standardReleaseOptions();

    const makeGrubImg = b.option(bool, "makeGrubImg", "Create GRUB bootable image");

    const boot = b.addObject("boot", "boot/multiboot2.zig");
    boot.setBuildMode(mode);
    boot.setTarget(target);
    boot.addPackagePath("arch", "arch/arch.zig");

    const interrupts = b.addObject("interrupts", "arch/x86/interrupts.zig");
    interrupts.setBuildMode(mode);
    interrupts.setTarget(target);

    const os = b.addExecutable("os", "kernel/x86/main.zig");
    os.step.dependOn(&interrupts.step);
    os.step.dependOn(&boot.step);

    os.setBuildMode(mode);
    os.setTarget(target);
    os.addObject(boot);
    os.addObject(interrupts);
    os.linker_script = std.build.FileSource{.path = "kernel/x86/link.ld"};
    os.addPackagePath("arch", "arch/arch.zig");
    os.addPackagePath("drivers", "drivers/drivers.zig");
    os.install();

    if (makeGrubImg != null and makeGrubImg.?)
        _ = try grubStep(b, os);
}

// TODO: We need to have bootable structure on the image, grub itself doesnt provide facilities to boot itself, so, we rely on -kernel right now
// TODO: For some reason grub-mkstandalone is called before elf file is ready sometimes?
pub fn grubStep(b: *std.build.Builder, elf: *std.build.LibExeObjStep) ![]const u8 {
    const elf_path = b.getInstallPath(elf.install_step.?.dest_dir, elf.out_filename);
    const grub_output = "zos.img";
    const grub_cfg_path = "boot/grub.cfg";
    // TODO: Should only be done under windows
    const grub_path = try b.exec(&[_][]const u8{"which", "grub-mkstandalone"});
    const grub_dir = std.fs.path.dirname(grub_path).?;
    const grub_i386_pc = try std.mem.join(b.allocator, "/", &[_][]const u8{grub_dir, "i386-pc"});
    const grub_i386_pc_win = try b.exec(&[_][]const u8{"cygpath", "-w", grub_i386_pc});
    const grub_step = b.addSystemCommand(&[_][]const u8{
        "grub-mkstandalone", "--format=i386-pc", b.fmt("--directory={s}", .{grub_i386_pc_win[0..grub_i386_pc_win.len - 1]}), "-o", grub_output,
        "--install-modules=normal multiboot2", "--themes=", "--locales=", "--fonts=",
        b.fmt("boot/zos.elf={s}", .{elf_path}),
        b.fmt("boot/grub/grub.cfg={s}", .{grub_cfg_path})
    });
    grub_step.step.dependOn(&elf.step);
    try grub_step.step.make();
    return grub_output;
}
