const std = @import("std");
const Selector = @import("selector.zig").Selector;

// TODO: Move to kernel directory?
// TODO: Define separate segment for kernel stack?

pub const page_size_in_bytes = 4096;
pub const kernel_stack_pages = 2;

pub const kernel_code_segment_idx = 0x08;
pub const kernel_data_segment_idx = 0x10;
pub const user_code_segment_idx = 0x18;
pub const user_data_segment_idx = 0x20;
pub const task_state_segment_idx = 0x28;

pub const kernel_code_selector = Selector{
    .privilege = .kernel,
    .table = .gdt,
    .idx = kernel_code_segment_idx,
};

// 0x00000 - 0x003ff | Real mode interrupt vector table
// 0x00400 - 0x004ff | BIOS data
// 0x00500 - 0x9ffff | Free to use, 7c00 is a boot sector base
// 0xa0000 - 0xbffff | Video memory
// 0xc0000 - 0xc7fff | Video BIOS
// 0xc8000 - 0xeffff | BIOS shadow area
// 0xf0000 - 0xfffff | System BIOS

pub const base: usize = 0x00100000;

pub var kernel_stack: [page_size_in_bytes * kernel_stack_pages]u8 align(16) linksection(".kernel_stack") = undefined;
