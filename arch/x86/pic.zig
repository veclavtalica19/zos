//! 8259 PIC interface

// https://wiki.osdev.org/PIC
// https://wiki.osdev.org/8259_PIC
// https://pdos.csail.mit.edu/6.828/2005/readings/hardware/8259A.pdf

const cpu = @import("cpu.zig");

pub const pic0_command_port = 0x20;
pub const pic0_data_port = 0x21;
pub const pic1_command_port = 0xa0;
pub const pic1_data_port = 0xa1;

pub const end_of_interrupt = 0x20;

pub const Line = enum(u8) {
    timer,
    keyboard,
    cascade, // Never raised
    com2,
    com1,
    lpt2,
    floppy,
    lpt1,
    cmos_clock,
    peripheral0,
    peripheral1,
    peripheral2,
    ps2_mouse,
    fpu,
    ata_harddrive0,
    ata_harddrive1
};

pub fn remap() void {
    const icw1_icw4 = 0x01;
    const icw1_init = 0x10;
    const icw4_8086 = 0x01;

    // Cache masks
    const mask0 = cpu.readPort(pic0_data_port);
    const mask1 = cpu.readPort(pic1_data_port);

    // TODO: Technically each IO op might require a bit of sleep on slower machines
    cpu.writePort(pic0_command_port, icw1_init | icw1_icw4);
    cpu.writePort(pic1_command_port, icw1_init | icw1_icw4);

    cpu.writePort(pic0_data_port, 0x20);
    cpu.writePort(pic1_data_port, 0x28);

    cpu.writePort(pic0_data_port, 1 << 2);
    cpu.writePort(pic1_data_port, 1 << 1);

    cpu.writePort(pic0_data_port, icw4_8086);
    cpu.writePort(pic1_data_port, icw4_8086);

    // Restore masks
    cpu.writePort(pic0_data_port, mask0);
    cpu.writePort(pic1_data_port, mask1);
}

// TODO: Kinda ugly
pub fn maskIrq(line_enum: Line) void {
    const l = @enumToInt(line_enum);
    const port: u8 = if (l < 8) pic0_data_port else pic1_data_port;
    const line: u3 = @intCast(u3, if (l < 8) l else l - 8);
    const value = cpu.readPort(port) & ~(@as(u8, 1) << line);
    cpu.writePort(port, value);
}

pub fn unmaskIrq(line_enum: Line) void {
    const l = @enumToInt(line_enum);
    const port: u8 = if (l < 8) pic0_data_port else pic1_data_port;
    const line: u3 = @intCast(u3, if (l < 8) l else l - 8);
    const value = cpu.readPort(port) & (@as(u8, 1) << line);
    cpu.writePort(port, value);
}
