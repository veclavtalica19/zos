const std = @import("std");
const idt = @import("idt.zig");
const gdt = @import("gdt.zig");
const fmt = std.fmt.comptimePrint;

// https://wiki.osdev.org/CPU_Registers_x86

pub const Register = enum {
    eax,
    ebx,
    ecx,
    edx,
    esi,
    edi,

    eip,

    esp,
    ebp,

    cs,
    ds,
    es,
    fs,
    gs,
    ss,

    eflags,

    cr0,
    cr1, // Reserved
    cr2,
    cr3,
    cr4,
    cr5, // Reserved
    cr6, // Reserved
    cr7, // Reserved
    cr8,
};

/// Packed registers from PUSHA
pub const RegisterPack = packed struct {
    edi: u32,
    esi: u32,
    ebp: u32,
    esp: u32,
    ebx: u32,
    edx: u32,
    ecx: u32,
    eax: u32,
};

pub const Flags = packed struct {
    // FLAGS
    carry_flag: u1,
    _reserved0: u1 = 1,
    parity_flag: u1,
    _reserved1: u1,
    adjust_flag: u1,
    _reserved2: u1,
    zero_flag: u1,
    sign_flag: u1,
    trap_flag: u1,
    interrupt_enable_flag: u1,
    direction_flag: u1,
    overflow_flag: u1,
    io_privilege: u1,
    nested_task_flag: u1,
    _reserved3: u1,
    // EFALGS
    resume_flag: u1,
    virtual_8086: u1,
    alignment_check: u1,
    virtual_interrupt_flag: u1,
    virtual_interrupt_pending: u1,
    cpuid_available: u1,
    _reserved4: u11,
};

pub const Cr0 = packed struct {
    protected_mode_enable: u1, // PE
    monitor_coprocessor: u1, // MP
    x87_fpu_emulation: u1, // EM
    task_switched: u1, // TS
    extension_type: u1, // ET
    numeric_error: u1, // NE
    _reserved0: u10,
    write_protect: u1, // WP
    _reserved1: u2,
    alignment_mask: u1, // AM
    _reserved2: u9,
    non_write_through: u1, // NW
    cache_disable: u1, // CD
    paging: u1, // PG
};

/// List of Intel defined exception interrupts
pub const Interrupts = enum(u32) {
    divide_by_zero,
    debug,
    non_maskable,
    breakpoint,
    overflow,
    bound_range_exceeded,
    invalid_opcode,
    device_not_found,
    double_fault,
    coprocessor_segment_overrun,
    invalis_tss,
    segment_not_present,
    stack_segment_fault,
    page_fault,
    _reserved0,
    fpu_exception,
    alignment_check,
    machine_check,
    simd_floating_point_exception,
    vmm_communication_exception,
    secutiry_exception,
    _reserved1,
};

pub inline fn clearFlags() void {
    asm volatile (
        \\ pushl $0
        \\ popf
        ::: "cc");
}

pub inline fn interrupt(comptime int: u8) void {
    if (int == 3) {
        asm volatile ("int3");
    } else {
        asm volatile ("int %[int]"
            :
            : [int] "n" (int),
        );
    }
}

// TODO: Allow clearing EFLAGS, GDT, IDT and LDT by the same function?
//       It would make sense to have generic interface like that
pub inline fn zeroRegister(comptime r: Register) void {
    const code = comptime fmt(
        \\ xor %%eax, %%eax
        \\ mov %%eax, %%{s}
    , .{@tagName(r)});
    asm volatile (code ::: "{eax}");
}

// TODO: Facilities to return 16 bit registers by anytype return type
pub inline fn getRegister(comptime r: Register) u32 {
    return switch (r) {
        .eax => asm (""
            : [ret] "={eax}" (-> u32),
        ),
        .ebx => asm (""
            : [ret] "={ebx}" (-> u32),
        ),
        else => unreachable,
    };
}

// TODO: Pass comptime known values in "n" input instead
pub inline fn setRegister(comptime r: Register, v: anytype) void {
    const code = comptime fmt("mov %[reg], %%{s}", .{@tagName(r)});
    asm volatile (code
        :
        : [reg] "r" (v),
    );
}

pub inline fn moveRegister(comptime src: Register, comptime dst: Register) void {
    const code = comptime fmt("mov %%{s}, %%{s}", .{ @tagName(src), @tagName(dst) });
    asm volatile (code);
}

pub inline fn enableInterrupts() void {
    asm volatile ("sti" ::: "{eflags}");
}

pub inline fn disableInterrupts() void {
    asm volatile ("cli" ::: "{eflags}");
}

// TODO: Should we assume that every register will be used with the same selector
pub inline fn loadDataSelector(ds: u16) void {
    asm volatile (
        \\ movw %%ax, %%ds
        \\ movw %%ax, %%es
        \\ movw %%ax, %%fs
        \\ movw %%ax, %%gs
        \\ movw %%ax, %%ss
        :
        : [val] "{ax}" (ds),
        : "{ds}", "{es}", "{fs}", "{gs}", "{ss}"
    );
}

// TODO: We can achieve runtime settable behavior by using indirect far jump
pub inline fn loadCodeSelector(comptime cs: u16) void {
    asm volatile (
        \\ ljmpl %[val], $0f
        \\ 0:
        :
        : [val] "n" (cs),
        : "{cs}"
    );
}

pub inline fn loadGdt(gdtr: *gdt.Register) void {
    asm volatile (
        \\ lgdt [gdtr]
        :
        : [gdtr] "r" (gdtr),
    );
}

pub inline fn loadIdt(idtr: *idt.Register) void {
    asm volatile (
        \\ lidt [idtr]
        :
        : [idtr] "r" (idtr),
    );
}

pub inline fn readPort(port: u16) u8 {
    return asm (
        \\ inb %[port], %[result]
        : [result] "={al}" (-> u8),
        : [port] "N{dx}" (port),
    );
}

pub inline fn writePort(port: u16, value: u8) void {
    asm volatile (
        \\ outb %[value], %[port]
        :
        : [value] "{al}" (value),
          [port] "N{dx}" (port),
    );
}

pub inline fn haltInLoop() noreturn {
    asm volatile (
        \\ 0:
        \\   cli
        \\   hlt
        \\   jmp 0b
    );
    unreachable;
}

/// Writes address of ELF symbol resolved on link time in given register
pub inline fn symbolAddrInRegister(comptime r: Register, comptime sym: []const u8) void {
    const code = comptime fmt("mov ${s}, %%{s}", .{ sym, @tagName(r) });
    asm volatile (code);
}

/// Returns address of ELF symbol resolved on link time
pub inline fn symbolAddr(comptime sym: []const u8) usize {
    return @ptrToInt(@extern(*usize, .{ .name = sym }));
}

// TODO: Ask for SS to setup?
pub inline fn setupStack(comptime addr: usize) void {
    asm volatile (
        \\ mov %[addr], %%esp
        \\ mov %[addr], %%ebp
        :
        : [addr] "n" (addr),
        : "{esp}", "{ebp}"
    );
}

comptime {
    const assert = @import("std").debug.assert;
    assert(@bitSizeOf(Flags) == 32);
    assert(@bitSizeOf(Cr0) == 32);
}
