//! x87 FPU implementation

// https://wiki.osdev.org/FPU

// TODO: Handle exceptions by interrupts

pub inline fn detect() bool {
    return asm (
        \\ mov %%cr0, %%eax
        \\ and $~0b1100, %%eax  // Clear TS and EM bits
        \\ mov %%eax, %%cr0
        \\ fninit               // Initialize Floating Point Unit
        \\ mov 0x1572, %%ax     // Store junk
        \\ fnstsw %%ax          // Store FPU status, it will be no-op if CPU isnt wired
        \\ cmp $0, %%ax         // Should be zeroed, or junk
        \\ jne 0f
        \\ mov $1, %%al
        \\ jmp 1f
        \\ 0:
        \\  mov $0, %%al
        \\ 1:
        : [ret] "={al}" (-> bool),
        :
        : "{eax}", "{flags}", "{cr0}"
    );
}

/// Make sure FPU is available before calling this
pub inline fn init() void {
    asm volatile (
        \\ mov %%cr0, %%eax
        \\ and $~0b1100, %%eax  // Clear TS, EM bits
        \\ or  $0b100010, %%eax // Set NE and MP
        \\ mov %%eax, %%cr0
        \\ fninit               // Initialize Floating Point Unit
        ::: "{eax}", "{flags}", "{cr0}");
}
