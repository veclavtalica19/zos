const std = @import("std");
const cpu = @import("arch").x86.cpu;
const isr = @import("arch").x86.isr;
const idt = @import("arch").x86.idt;
const gdt = @import("arch").x86.gdt;
const fpu = @import("arch").x86.fpu;
const pic = @import("arch").x86.pic;
const layout = @import("arch").x86.layout;
const vga = @import("drivers").vga.printer;
const tests = @import("tests.zig");
const Printer = @import("drivers").printer.Printer;

var memory_table align(8) = [_]gdt.Descriptor{
    std.mem.zeroes(gdt.Descriptor),
    gdt.Descriptor.init(0x0, 0xfffff, gdt.Access.init_code_segment(.readable, .strict, .kernel, .present), gdt.Flags.init(.protected, .page)),
    gdt.Descriptor.init(0x0, 0xfffff, gdt.Access.init_data_segment(.writable, .grows_up, .kernel, .present), gdt.Flags.init(.protected, .page)),
};

var interrupt_table: [48]idt.Descriptor align(8) = undefined;

var writer = Printer.wrap(&vga.printer).writer();

export fn kernelMain() noreturn {
    @setAlignStack(16);

    // TODO: Zero LDT as we dont use it

    // Flush TLB
    cpu.zeroRegister(.cr4);

    // TODO: Save BIOS GDT/IDT vectors for future use

    zeroBss();

    // if (!fpu.detect()) {
    // @panic("FPU isn't detected");
    // }

    // fpu.init();
    // vga.init();

    // writer.context.stateClear();

    gdt.load(memory_table[0..]);
    cpu.loadCodeSelector(layout.kernel_code_segment_idx);
    cpu.loadDataSelector(layout.kernel_data_segment_idx);

    isr.setDefaults(interrupt_table[0..48]);
    idt.load(interrupt_table[0..]);
    pic.remap();

    // tests.run(&writer);

    cpu.enableInterrupts();
    cpu.interrupt(3);

    cpu.haltInLoop();
}

pub fn panic(msg: []const u8, stacktrace: ?*std.builtin.StackTrace) noreturn {
    @setCold(true);

    writer.context.stateSetColor(.white, .blue);
    try writer.print("kernel panic occurred!\nmessage: {s}\n", .{msg});

    // TODO: Handle stack unwinding and printing of information
    // We could probably embed DWARF file in rodata and look it up
    if (stacktrace) |trace| {
        for (trace.instruction_addresses) |address| {
            try writer.print("{any}", .{address});
        }
    } else {
        try writer.writeAll("stack trace is unavailable!");
    }
    cpu.haltInLoop();
}

fn zeroBss() void {
    const bss_start = cpu.symbolAddr("__bss_start");
    const bss_end = cpu.symbolAddr("__bss_end");
    @memset(@intToPtr([*]u8, bss_start), 0, bss_end - bss_start);
}
